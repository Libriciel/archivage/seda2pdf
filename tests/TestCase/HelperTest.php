<?php
/**
 * Seda2Pdf\Test\TestCase\HelperTest
 */

namespace Seda2Pdf\Test\TestCase;

use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use PHPUnit\Framework\TestCase;
use Seda2Pdf\Exception\XPathQueryException;
use Seda2Pdf\Helper;
use Seda2Pdf\Seda2Pdf;

class HelperTest extends TestCase
{
    public function testGet()
    {
        $helper = $this->getHelper();

        $archive = $helper->get('Archive');
        $this->assertInstanceOf(DOMElement::class, $archive);

        $archiveObject = $helper->get('ArchiveObject', $archive);
        $this->assertInstanceOf(DOMElement::class, $archiveObject);

        $this->assertEquals($archiveObject, $helper->get('Archive.ArchiveObject'));
        $this->assertNull($helper->get('Foo.Bar'));
    }

    public function testGetAll()
    {
        $helper = $this->getHelper();

        $archiveObjects = $helper->getAll('Archive.ArchiveObject.ArchiveObject');
        $this->assertInstanceOf(DOMNodeList::class, $archiveObjects);

        $this->assertCount(2, $archiveObjects);
        $this->assertCount(0, $helper->getAll('Foo.Bar'));

        $this->expectException(XPathQueryException::class);
        $helper->getAll('bad:Archive');
    }

    public function testGetValue()
    {
        $helper = $this->getHelper();

        $sa = $helper->getValue('ArchivalAgency.Identification');
        $this->assertEquals('sa', $sa);

        $this->assertNull($helper->getValue('Foo.Bar'));
    }

    public function testDisplayValue()
    {
        $helper = $this->getHelper();

        $title = 'foobar';
        $sa = $helper->displayValue($title, 'ArchivalAgency.Identification');
        $this->assertStringContainsString($title, $sa);
        $this->assertStringContainsString('sa', $sa);

        $this->assertNull($helper->displayValue($title, 'Foo.Bar'));
    }

    public function testDisplayAttrValue()
    {
        $helper = $this->getHelper();

        $title = 'foobar';
        $filename = $helper->displayAttrValue(
            $title,
            'Archive.ArchiveObject.Document.Attachment',
            'filename'
        );
        $this->assertStringContainsString($title, $filename);
        $this->assertStringContainsString(
            'test_transfer_structure/file1.txt',
            $filename
        );

        $this->assertNull($helper->displayAttrValue($title, 'Foo.Bar', 'filename'));
    }

    public function testGetAttrValue()
    {
        $helper = $this->getHelper();

        $filename = $helper->getAttrValue(
            'Archive.ArchiveObject.Document.Attachment',
            'filename'
        );
        $this->assertEquals('test_transfer_structure/file1.txt', $filename);

        $this->assertNull($helper->getAttrValue('Foo.Bar', 'filename'));
    }

    public function testGetDateValue()
    {
        $helper = $this->getHelper();

        $date = $helper->getDateValue('Date');
        $this->assertEquals('07/12/2020', $date);

        $this->assertNull($helper->getDateValue('Foo.Bar'));

        // valeur multiples
        $dom = new DOMDocument();
        $dom->loadXML(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<test xmlns="https://test">
    <date>2021-01-01</date>
    <date>2022-01-01</date>
</test>
XML
        );
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', 'https://test');
        $helper = new Helper($xpath);
        $dates = $helper->getDateValue('date');
        $this->assertStringContainsString('2021', $dates);
        $this->assertStringContainsString('2022', $dates);
        $this->assertStringContainsString('<ul>', $dates);
    }

    public function testGetDatetimeValue()
    {
        $helper = $this->getHelper();

        $date = $helper->getDatetimeValue('Date');
        $this->assertEquals('07/12/2020 16:10:00', $date);

        $this->assertNull($helper->getDatetimeValue('Foo.Bar'));

        // valeur multiples
        $dom = new DOMDocument();
        $dom->loadXML(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<test xmlns="https://test">
    <datetime>2021-01-01T11:15:12</datetime>
    <datetime>2022-01-01T10:10:55</datetime>
</test>
XML
        );
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', 'https://test');
        $helper = new Helper($xpath);
        $dates = $helper->getDatetimeValue('datetime');
        $this->assertStringContainsString('2021', $dates);
        $this->assertStringContainsString('2022', $dates);
        $this->assertStringContainsString('<ul>', $dates);
    }

    public function testGetSizeValue()
    {
        $helper = $this->getHelper();

        $date = $helper->getSizeValue('Archive.ArchiveObject.Document.Size');
        $this->assertEquals('5o', $date);

        $this->assertNull($helper->getSizeValue('Foo.Bar'));

        // valeur multiples
        $dom = new DOMDocument();
        $dom->loadXML(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<test xmlns="https://test">
    <size>15432</size>
    <size>17894521</size>
</test>
XML
        );
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', 'https://test');
        $helper = new Helper($xpath);
        $size = $helper->getSizeValue('size');
        $this->assertStringContainsString('15.07Ko', $size);
        $this->assertStringContainsString('17.07Mo', $size);
        $this->assertStringContainsString('<ul>', $size);
    }

    public function testGetTextValue()
    {
        // valeur multiples
        $dom = new DOMDocument();
        $dom->loadXML(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<test xmlns="https://test">
    <txt>test</txt>
    <txt>toto</txt>
</test>
XML
        );
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', 'https://test');
        $helper = new Helper($xpath);
        $size = $helper->getTextValue('txt');
        $this->assertStringContainsString('test', $size);
        $this->assertStringContainsString('toto', $size);
        $this->assertStringContainsString('<ul>', $size);
    }

    public function testReadableSize()
    {
        $helper = $this->getHelper();

        $this->assertEquals('124o', $helper->readableSize(124));
        $this->assertEquals('15Ko', $helper->readableSize(15 * 1024));
        $this->assertEquals('118Mo', $helper->readableSize(118 * 1024 * 1024));
        $this->assertEquals('2.5Go', $helper->readableSize(2.5 * 1024 * 1024 * 1024));
    }

    public function testRender()
    {
        $seda2pdf = new Seda2Pdf(TEST_DATA.'seda10.xml');
        $helper = $seda2pdf->helper;
        $dir = getcwd();
        chdir(dirname($seda2pdf->template));
        $lv = ob_get_level();
        ob_start();
        try {
            $helper->render('archive.php', '', 1, $helper->get('Archive'));
            $content = ob_get_clean();
        } finally {
            chdir($dir);
            while (ob_get_level() > $lv) {
                ob_end_flush();
            }
        }
        /** @noinspection HtmlDeprecatedAttribute */
        $this->assertStringContainsString('<h2>1.', $content);
    }

    public function testTable()
    {
        $helper = $this->getHelper();
        $output = $helper->table(
            [
                'Date' => [
                    'type' => 'datetime',
                    'label' => 'date',
                ],
                'Archive.ArchiveObject.Document.Size' => [
                    'type' => 'size',
                    'label' => 'Size',
                ],
                'Archive.AccessRestrictionRule.StartDate' => [
                    'type' => 'date',
                    'label' => 'StartDate',
                ],
                'Archive.ArchiveObject.Document.Attachment@filename' => [
                    'type' => 'attribute',
                    'label' => 'Filename',
                ],
                'ArchivalAgency.Identification' => 'ArchivalAgency',
            ]
        );
        $this->assertStringContainsString('<tr><th>date</th><td>07/12/2020 16:10:00</td></tr>', $output);
        $this->assertStringContainsString('<tr><th>Size</th><td>5o</td></tr>', $output);
        $this->assertStringContainsString('<tr><th>StartDate</th><td>07/12/2020</td></tr>', $output);
        $this->assertStringContainsString('<tr><th>Filename</th><td>test_transfer_structure/file1.txt</td></tr>', $output);
        $this->assertStringContainsString('<tr><th>ArchivalAgency</th><td>sa</td></tr>', $output);
    }

    public function testMetadataParser()
    {
        $helper = $this->getHelper();
        $result = $helper->metadataParser(<<<EOT
    foo: bar
    bar: 1.65452
EOT
);
        $expected = [
            'foo' => 'bar',
            'bar' => '1.65452',
        ];
        $this->assertEquals($expected, $result);
    }

    private function getHelper(): Helper
    {
        $dom = new DOMDocument;
        $dom->load(TEST_DATA.'seda10.xml');
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
        return new Helper($xpath);
    }
}