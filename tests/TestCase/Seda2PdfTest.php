<?php
/**
 * Seda2Pdf\Test\TestCase\Seda2PdfTest
 */

namespace Seda2Pdf\Test\TestCase;

use DOMDocument;
use Exception;
use PHPUnit\Framework\TestCase;
use Seda2Pdf\Exception\NotADOMDocumentException;
use Seda2Pdf\Exception\NSNotSupportedException;
use Seda2Pdf\Exception\WkhtmltopdfFailedException;
use Seda2Pdf\Seda2Pdf;

class Seda2PdfTest extends TestCase
{
    public function testConstrucct()
    {
        $utility = new Seda2Pdf(TEST_DATA.'seda10.xml');
        $this->assertInstanceOf(Seda2Pdf::class, $utility);

        $utility = new Seda2Pdf(file_get_contents(TEST_DATA.'seda10.xml'));
        $this->assertInstanceOf(Seda2Pdf::class, $utility);

        $dom = new DOMDocument;
        $dom->load(TEST_DATA.'seda10.xml');
        $utility = new Seda2Pdf($dom);
        $this->assertInstanceOf(Seda2Pdf::class, $utility);

        $e = null;
        try {
            new Seda2Pdf(['foo' => 'bar']);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(NotADOMDocumentException::class, $e);

        $e = null;
        try {
            new Seda2Pdf(new DOMDocument);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(NSNotSupportedException::class, $e);
    }

    public function testGenerate()
    {
        $pdf = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
        $utility = new Seda2Pdf(TEST_DATA.'seda10.xml');
        $utility->generate($pdf);
        $this->assertFileExists($pdf);
        $this->assertNotEquals(0, filesize($pdf));
        $this->assertEquals('application/pdf', mime_content_type($pdf));
        unlink($pdf);

        $content = $utility->generate();
        $this->assertNotEquals(0, strlen($content));

        $e = null;
        try {
            $utility->generate('/not-writable/failed.pdf');
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(WkhtmltopdfFailedException::class, $e);
    }

    public function testRenderHtml()
    {
        $utility = new Seda2Pdf(TEST_DATA.'seda10.xml');
        $html = $utility->renderHtml();
        $this->assertStringContainsString('<html', $html);

        $utility->template = 'do_not_exists.php';
        $e = null;
        try {
            $utility->renderHtml();
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(Exception::class, $e);
    }
}
