# Seda2Pdf
[![License](https://img.shields.io/badge/license-AGPL-blue)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![pipeline status](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/-/commits/master)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/seda2pdf/index.html)
[![Maintenabilité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=Seda2Pdf&metric=sqale_rating&token=sqb_177586747afead781559fa250c9a3605ce1374a4)](https://sonarqube.libriciel.fr/dashboard?id=Seda2Pdf)
[![Quality Gate Status](https://sonarqube.libriciel.fr/api/project_badges/measure?project=Seda2Pdf&metric=alert_status&token=sqb_177586747afead781559fa250c9a3605ce1374a4)](https://sonarqube.libriciel.fr/dashboard?id=Seda2Pdf)

## Installation

```
sudo apt install xvfb xfonts-75dpi xfonts-base
wget https://ressources.libriciel.fr/deploiement/w/wkhtmltox-0.12.6-1.focal-amd64.deb
sudo dpkg -i wkhtmltox-0.12.6-1.focal-amd64.deb
wkhtmltopdf --version
```

```
composer config repositories.libriciel/seda2pdf git https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf.git
composer require libriciel/seda2pdf
```

## Utilisation

```php
use Seda2Pdf\Seda2Pdf;

$generator = new Seda2Pdf($sedaFilename);
$generator->generate($destinationFilename);
```
