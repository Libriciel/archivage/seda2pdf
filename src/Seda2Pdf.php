<?php
/**
 * Seda2Pdf\Seda2Pdf
 */

namespace Seda2Pdf;

use DOMDocument;
use DOMXPath;
use Seda2Pdf\Exception\NotADOMDocumentException;
use Seda2Pdf\Exception\NSNotSupportedException;
use Seda2Pdf\Exception\WkhtmltopdfFailedException;
use Throwable;

/**
 * Permet de transformer un fichier SEDA en fichier PDF
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda2Pdf
{
    const NS_SEDA_V02 = 'fr:gouv:ae:archive:draft:standard_echange_v0.2';
    const NS_SEDA_V10 = 'fr:gouv:culture:archivesdefrance:seda:v1.0';
    const NS_SEDA_V21 = 'fr:gouv:culture:archivesdefrance:seda:v2.1';
    const NS_SEDA_V22 = 'fr:gouv:culture:archivesdefrance:seda:v2.2';

    const MSG_ARCHIVE = 'Archive';
    const MSG_ARCHIVE_TRANSFER = 'ArchiveTransfer';
    const MSG_DELIVERY_REQUEST = 'ArchiveDeliveryRequest';
    const MSG_AUTH_ORI_REQUEST = 'AuthorizationOriginatingAgencyRequest';

    const SEDA_TEMPLATES = [
        self::NS_SEDA_V02 => [
            self::MSG_ARCHIVE => '../templates/seda02/base_archive.php',
            self::MSG_ARCHIVE_TRANSFER => '../templates/seda02/base_transfer.php',
        ],
        self::NS_SEDA_V10 => [
            self::MSG_ARCHIVE => '../templates/seda10/base_archive.php',
            self::MSG_ARCHIVE_TRANSFER => '../templates/seda10/base_transfer.php',
        ],
        self::NS_SEDA_V21 => [
            self::MSG_ARCHIVE => '../templates/seda2x/base_archive.php',
            self::MSG_DELIVERY_REQUEST => '../templates/seda2x/base_delivery_request.php',
            self::MSG_ARCHIVE_TRANSFER => '../templates/seda2x/base_transfer.php',
            self::MSG_AUTH_ORI_REQUEST => '../templates/seda2x/base_destruction_request.php',
        ],
        self::NS_SEDA_V22 => [
            self::MSG_ARCHIVE => '../templates/seda2x/base_archive.php',
            self::MSG_DELIVERY_REQUEST => '../templates/seda2x/base_delivery_request.php',
            self::MSG_ARCHIVE_TRANSFER => '../templates/seda2x/base_transfer.php',
            self::MSG_AUTH_ORI_REQUEST => '../templates/seda2x/base_destruction_request.php',
        ],
    ];

    /**
     * @var DOMDocument
     */
    public $dom;
    /**
     * @var string
     */
    public $namespace;
    /**
     * @var string
     */
    public $template;
    /**
     * @var Helper
     */
    public $helper;
    /**
     * @var DOMXPath
     */
    private DOMXPath $xpath;

    /**
     * Seda2Pdf constructor.
     * @param string|DOMDocument $seda
     * @throws NotADOMDocumentException|NSNotSupportedException
     */
    public function __construct($seda)
    {
        if (is_string($seda) && is_file($seda)) {
            $path = $seda;
            $seda = new DOMDocument;
            $seda->load($path);
        } elseif (is_string($seda)) {
            $xml = $seda;
            $seda = new DOMDocument;
            $seda->loadXML($xml);
            unset($xml);
        }
        if (!$seda instanceof DOMDocument) {
            throw new NotADOMDocumentException('$seda must be a DOMDocument');
        }
        $this->dom = $seda;
        $xmlns = $seda->documentElement ? $seda->documentElement->getAttributeNode('xmlns') : null;
        $this->namespace = $xmlns ? $xmlns->nodeValue : null;
        $message = $seda->documentElement ? $seda->documentElement->nodeName : null;
        if (!$this->namespace
            || !$message
            || !isset(self::SEDA_TEMPLATES[$this->namespace][$message])
        ) {
            throw new NSNotSupportedException('this namespace is not supported');
        }
        $this->template = realpath(
            __DIR__.DIRECTORY_SEPARATOR.self::SEDA_TEMPLATES[$this->namespace][$message]
        );
        $this->xpath = new DOMXPath($this->dom);
        $this->xpath->registerNamespace('ns', $this->namespace);
        $this->helper = new Helper($this->xpath);
    }

    /**
     * Renvoi le pdf construit
     * @param string|null $destination
     * @return string|null $destination ou le contenu du pdf
     * @throws WkhtmltopdfFailedException
     */
    public function generate(string $destination = null): ?string
    {
        $html = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
        file_put_contents($html, $this->renderHtml());
        $pdf = $destination ?: tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
        exec(
            sprintf(
                'cat %s | xvfb-run -a --server-args="-screen 0, 1024x768x24" wkhtmltopdf - %s 2>&1',
                escapeshellarg($html),
                escapeshellarg($pdf)
            ),
            $out,
            $code
        );
        if ($code !== 0) {
            throw new WkhtmltopdfFailedException(var_export($out, true));
        }
        unlink($html);
        if ($destination) {
            return $destination;
        }
        $content = file_get_contents($pdf);
        unlink($pdf);
        return $content;
    }

    /**
     * Renvoi le html
     */
    public function renderHtml(): string
    {
        return $this->evaluateTemplate(
            $this->template,
            [
                'dom' => $this->dom,
                'xpath' => $this->xpath,
                'helper' => $this->helper,
            ]
        );
    }

    /**
     * Sandbox method to evaluate a template / view script in.
     *
     * @param string $templateFile Filename of the template.
     * @param array $dataForView Data to include in rendered view.
     * @return string Rendered output
     */
    private function evaluateTemplate(string $templateFile, array $dataForView): string
    {
        $dir = getcwd();
        chdir(dirname($templateFile));
        extract($dataForView);

        $bufferLevel = ob_get_level();
        ob_start();

        try {
            include $templateFile; // NOSONAR pas de include_once ici
        } catch (Throwable $exception) {
            while (ob_get_level() > $bufferLevel) {
                ob_end_clean();
            }

            throw $exception;
        } finally {
            chdir($dir);
        }

        return ob_get_clean();
    }
}
