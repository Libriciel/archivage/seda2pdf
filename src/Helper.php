<?php
/**
 * Seda2Pdf\Helper
 */

namespace Seda2Pdf;

use DateTime;
use DOMXpath;
use DOMElement;
use DOMNodeList;
use Exception;
use Seda2Pdf\Exception\XPathQueryException;

/**
 * Facilite le templating
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Helper
{
    const DATE_FORMAT = 'd/m/Y';
    const DATETIME_FORMAT = 'd/m/Y H:i:s';
    public DOMXpath $xpath;

    /**
     * Helper constructor.
     * @param DOMXpath $xpath
     */
    public function __construct(DOMXpath $xpath)
    {
        $this->xpath = $xpath;
    }

    /**
     * Donne l'element par un chemin
     * ex: ArchiveTransfer.ArchivalAgency.Name
     * @param string          $path
     * @param DOMElement|null $from
     * @return DOMElement|null
     * @throws XPathQueryException
     */
    public function get(string $path, DOMElement $from = null): ?DOMElement
    {
        $list = $this->getAll($path, $from);
        $item = $list->item(0);
        return $item instanceof DOMElement ? $item : null;
    }

    /**
     * Donne l'element par un chemin
     * ex: ArchiveTransfer.ArchivalAgency.Name
     * @param string          $path
     * @param DOMElement|null $from
     * @return DOMNodeList
     * @throws XPathQueryException
     */
    public function getAll(string $path, DOMElement $from = null): DOMNodeList
    {
        $elements = explode('.', $path);
        $path = 'ns:'.implode('/ns:', $elements);
        $list = @$this->xpath->query($path, $from);
        if (!$list instanceof DOMNodeList) {
            throw new XPathQueryException($path.' is not a valid query');
        }
        return $list;
    }

    /**
     * Donne la valeur d'un noeud
     * @param string          $path
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function getValue(string $path, DOMElement $from = null): ?string
    {
        $node = $this->get($path, $from);
        /** @noinspection PhpExpressionAlwaysNullInspection bug IDE */
        return $node ? $node->nodeValue : null;
    }

    /**
     * Donne la valeur d'un noeud
     * @param string          $title
     * @param string          $path
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function displayValue(string $title, string $path, DOMElement $from = null): ?string
    {
        $value = $this->getValue($path, $from);
        return $value
            ? "<dt>$title</dt><dd>$value</dd>\n"
            : null;
    }

    /**
     * Donne la valeur d'un noeud
     * @param string          $title
     * @param string          $path
     * @param string          $attribute
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function displayAttrValue(string $title, string $path, string $attribute, DOMElement $from = null): ?string
    {
        $value = $this->getAttrValue($path, $attribute, $from);
        return $value
            ? "<dt>$title</dt><dd>$value</dd>\n"
            : null;
    }

    /**
     * Donne la valeur d'un noeud
     * @param string          $path
     * @param string          $attribute
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function getAttrValue(string $path, string $attribute, DOMElement $from = null): ?string
    {
        $node = $this->get($path, $from);
        return $node ? $node->getAttribute($attribute) : null;
    }

    /**
     * Donne la (ou les) valeur(s) d'un noeud sous forme de date (ul si multiple)
     * @param string          $path
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function getDateValue(string $path, DOMElement $from = null): ?string
    {
        $all = $this->getAll($path, $from);
        if ($all->count() > 1) {
            $str = '';
            /** @var DOMElement $elem */
            foreach ($all as $elem) {
                $str .= $this->wrap('li', $this->getDate($elem->nodeValue));
            }
            return $this->wrap('ul', $str);
        }

        return $this->getDate($this->getValue($path, $from));
    }

    /**
     * Formatage d'un champs date
     * @param string|null $value
     * @return string|null
     * @throws Exception
     */
    protected function getDate($value): ?string
    {
        $date = $value ? new DateTime($value) : null;
        return $date ? $date->format(self::DATE_FORMAT) : null;
    }

    /**
     * Donne la (ou les) valeur(s) d'un noeud sous forme de datetime (ul si multiple)
     * @param string          $path
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function getDatetimeValue(string $path, DOMElement $from = null): ?string
    {
        $all = $this->getAll($path, $from);
        if ($all->count() > 1) {
            $str = '';
            /** @var DOMElement $elem */
            foreach ($all as $elem) {
                $str .= $this->wrap('li', $this->getDateTime($elem->nodeValue));
            }
            return $this->wrap('ul', $str);
        }

        return $this->getDateTime($this->getValue($path, $from));
    }

    /**
     * Formatage d'un champs datetime
     * @param string|null $value
     * @return string|null
     * @throws Exception
     */
    protected function getDateTime($value): ?string
    {
        $date = $value ? new DateTime($value) : null;
        return $date ? $date->format(self::DATETIME_FORMAT) : null;
    }

    /**
     * Donne la (ou les) valeur(s) d'un noeud sous forme de o/Ko/Mo/Go (ul si multiple)
     * @param string          $path
     * @param DOMElement|null $from
     * @return string|null
     * @throws XPathQueryException
     */
    public function getSizeValue(string $path, DOMElement $from = null): ?string
    {
        $all = $this->getAll($path, $from);
        if ($all->count() > 1) {
            $str = '';
            /** @var DOMElement $elem */
            foreach ($all as $elem) {
                $str .= $this->wrap('li', $this->getSize($elem->nodeValue));
            }
            return $this->wrap('ul', $str);
        }

        return $this->getSize($this->getValue($path, $from));
    }

    /**
     * Formatage champ size
     * @param string|null $value
     * @return string|null
     * @throws Exception
     */
    protected function getSize($value): ?string
    {
        return $value ? $this->readableSize($value) : null;
    }

    /**
     * Donne la ou les valeur(s) d'un noeud texte (ul si multiple)
     * @param string          $path
     * @param DOMElement|null $from
     * @return string
     * @throws XPathQueryException
     */
    public function getTextValue($path, DOMElement $from = null): ?string
    {
        $all = $this->getAll($path, $from);
        if ($all->count() > 1) {
            $str = '';
            /** @var DOMElement $elem */
            foreach ($all as $elem) {
                $str .= $this->wrap('li', $this->getText($elem->nodeValue));
            }
            return $this->wrap('ul', $str);
        }

        return $this->getText($this->getValue($path, $from));
    }

    /**
     * Formatage d'un champs texte
     * @param string|null $value
     * @return string|null
     * @throws Exception
     */
    protected function getText($value): ?string
    {
        return $value ? nl2br(htmlentities($value)) : null;
    }

    /**
     * Rend lisible une taille de fichier
     * @param int $size
     * @return string
     */
    public function readableSize(int $size): string
    {
        $suffix = ['o', 'Ko', 'Mo', 'Go'];
        $s = 0;
        while ($size > 1024 && $s < 3) {
            $size /= 1024;
            $s++;
        }
        return round($size, 2).$suffix[$s];
    }

    /**
     * inclue un template avec un path pour suivre la structure
     * ex:  1. Archive
     *      1.1 ArchiveObject
     *      1.2. ArchiveObject
     *      1.2.1 Document
     *      1.2.2 Document
     * @param string          $template
     * @param string          $path
     * @param int             $index
     * @param DOMElement|null $parent
     */
    public function render(string $template, string $path, int $index, ?DOMElement $parent)
    {
        $templateVars = [
            'helper' => $this,
            'path' => $path,
            'index' => $index,
            'parent' => $parent,
        ];
        extract($templateVars); // disable "unused var" warning
        include $template; // NOSONAR pas de include_once ici
    }

    /**
     * Affiche un tableau de résultats
     * @param array           $table
     * @param DOMElement|null $from
     * @return string
     * @throws XPathQueryException
     */
    public function table(array $table, DOMElement $from = null): string
    {
        $trs = [];
        foreach ($table as $element => $params) {
            if (is_string($params)) {
                $params = ['label' => $params, 'type' => 'text'];
            }
            $label = $params['label'] ?? $element;
            switch ($params['type'] ?? 'text') {
                case 'date':
                    $value = $this->getDateValue($element, $from);
                    break;
                case 'datetime':
                    $value = $this->getDatetimeValue($element, $from);
                    break;
                case 'size':
                    $value = $this->getSizeValue($element, $from);
                    break;
                case 'attribute':
                    [$element, $attribute] = explode('@', $element);
                    $value = $this->getAttrValue($element, $attribute, $from);
                    break;
                case 'text':
                default:
                    $value = $this->getTextValue($element, $from);
            }
            if ($value) {
                $trs[] = "<tr><th>$label</th><td>$value</td></tr>\n";
            }
        }
        return $trs ? '<table><tbody>'.implode("\n", $trs).'</tbody></table>' : '';
    }

    /**
     * Transforme une liste de clé -> valeurs en array
     * ex:
     * <Node>
     *      foo: bar
     *      baz: biz
     * </Node>
     * return [
     *      'foo' => 'bar',
     *      'baz' => 'biz',
     * ]
     * @param string $nodeValue
     * @return array
     */
    public function metadataParser(string $nodeValue): array
    {
        $rawMetadata = explode(PHP_EOL, trim($nodeValue));
        $metadata = [];
        foreach ($rawMetadata as $meta) {
            if (strpos($meta, ':')) {
                [$key, $value] = explode(':', $meta, 2);
                $metadata[trim($key)] = trim($value);
            }
        }
        return $metadata;
    }

    /**
     * Donne un element LI
     * @param string      $tag
     * @param string|null $str
     * @return string
     */
    private function wrap(string $tag, string $str = null): string
    {
        return sprintf("<%s>%s</%s>\n", $tag, $str, $tag);
    }
}
