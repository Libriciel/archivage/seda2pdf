<?php
/**
 * Seda2Pdf\Exception\NSNotSupportedException
 */

namespace Seda2Pdf\Exception;

use Exception;

/**
 * Le type de message SEDA n'est pas pris en charge
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class NSNotSupportedException extends Exception
{
}
