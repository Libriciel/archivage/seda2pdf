<?php
/**
 * Seda2Pdf\Exception\NotADOMDocumentException
 */

namespace Seda2Pdf\Exception;

use Exception;

/**
 * N'est pas un DOMDocument
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class NotADOMDocumentException extends Exception
{
}
