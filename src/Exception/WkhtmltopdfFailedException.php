<?php
/**
 * Seda2Pdf\Exception\WkhtmltopdfFailedException
 */

namespace Seda2Pdf\Exception;

use Exception;

/**
 * Echec de l'executable wkhtmltopdf
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class WkhtmltopdfFailedException extends Exception
{
}
