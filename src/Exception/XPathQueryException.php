<?php
/**
 * Seda2Pdf\Exception\XPathQueryException
 */

namespace Seda2Pdf\Exception;

use Exception;

/**
 * Mauvais query xpath
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class XPathQueryException extends Exception
{
}
