<?php
/**
 * @var Seda2Pdf\Helper $helper
 */

$elements = [
    'StorageRule' => "Gestion de la durée d'utilité courante",
    'AppraisalRule' => "Gestion de la durée d'utilité administrative",
    'AccessRule' => "Gestion de la communicabilité",
    'DisseminationRule' => "Gestion de la diffusion",
    'ReuseRule' => "Gestion de la réutilisation",
    'ClassificationRule' => "Gestion de la classification",
];
foreach ($elements as $element => $label):
    if ($helper->getAll($element, $parent)->count()): ?>

<div class="section">
    <h3 class="h2"><?=$label?></h3>

    <?=$helper->table(
        [
            'Rule' => "Règle",
            'StartDate' =>  [
                'label' => "Date de départ",
                'type' => 'date',
            ],
            'FinalAction' => "Action terme de la durée de gestion",
            'ClassificationAudience' => "Audience de la classification",
            'ClassificationLevel' => "Niveau de classification",
            'ClassificationOwner' => "Propriétaire de la classification",
            'ClassificationReassessingDate' => "Réévaluation de la classification",
            'NeedReassessingAuthorization' => "Autorisation de réévaluation de la classification",
        ],
        $helper->get($element, $parent)
    )?>
</div>

    <?php endif; ?>

<?php endforeach; ?>

<?php if ($helper->getAll('LogBook', $parent)->count()): ?>
<div class="section">
    <h3 class="h2">Journal des traces</h3>

    <?php foreach ($helper->getAll('LogBook.Event', $parent) as $event): ?>

    <div class="section">
        <h4 class="h2">Événement</h4>

        <?=$this->table(
            [
                'EventIdentifier' => "Identifiant",
                'EventTypeCode' => "Code",
                'EventType' => "Type",
                'EventDateTime' =>  [
                    'label' => "Date",
                    'type' => 'datetime',
                ],
                'EventDetail' => "Détail",
                'Outcome' => "Résultat",
                'OutcomeDetail' => "Description détaillée du résultat",
                'OutcomeDetailMessage' => "Message complet de description du résultat",
                'EventDetailData' => "Message technique détaillant l'événement",
            ],
            $event
        )?>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>
