<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
$managementMetadata = $helper->get('ManagementMetadata', $parent ?? null);
?>

<h2 class="h2">Métadonnées de gestion</h2>

<?=$helper->table(
    [
        'ArchivalProfile' => "Profil d'archives",
        'ServiceLevel' => "Niveau de service",
        'AcquisitionInformation' => "Modalités d'entrée",
        'LegalStatus' => "Statut des archives échangées",
        'OriginatingAgencyIdentifier' => "Identifiant du service producteur",
        'SubmissionAgencyIdentifier' => "Identifiant du service versant",
    ],
    $managementMetadata
)?>

<?php $helper->render('management_group.php', $path, 0, $managementMetadata); ?>

<h2 class="h2">Unités d'archives</h2>

<?php
$descriptiveMetadata = $helper->get('DescriptiveMetadata', $parent);
$archiveUnitKey = 0;
foreach ($helper->getAll('ArchiveUnit', $descriptiveMetadata) as $archiveUnit) {
    $archiveUnitKey++;
    $helper->render('archive_unit.php', '', $archiveUnitKey, $archiveUnit);
}
?>

<h2 class="h2">Fichiers</h2>

<?php
$archiveBinaryKey = 0;
foreach ($helper->xpath->query('//ns:BinaryDataObject') as $archiveBinary) {
    $archiveBinaryKey++;
    $helper->render('binary.php', '', $archiveBinaryKey, $archiveBinary);
}
