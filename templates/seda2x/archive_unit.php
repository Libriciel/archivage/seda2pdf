<?php
/**
 * @var Seda2Pdf\Helper $helper
 * @var DOMElement $parent
 */
?>
<h3 class="h2">
    <a id="<?=$parent->getAttribute('id')?>">
        <?=ltrim("$path.$index", '.')?>. - <?=$helper->getValue('Content.Title', $parent)?>
    </a>
</h3>

<?=$helper->table(
    [
        'ArchiveUnitProfile' => "Profil d'Unité d'Archive",
        'Content.DescriptionLevel' => "Niveau de Description",
        'Content.Title' => "Titre",
        'Content.FilePlanPosition' => "Position dans le plan de classement",
        'Content.SystemId' => "Identifiant attribué aux objets par le SAE",
        'Content.OriginatingSystemId' => "Identifiant système attribué à l'ArchiveUnit par l'application du service producteur",
        'Content.ArchivalAgencyArchiveUnitIdentifier' => "Identifiant métier attribué à l'ArchiveUnit par le service d'archives",
        'Content.OriginatingAgencyArchiveUnitIdentifier' => "Identifiant métier attribué à l'ArchiveUnit par le service producteur",
        'Content.TransferringAgencyArchiveUnitIdentifier' => "Identifiant attribué à l'ArchiveUnit par le service versant",
        'Content.Description' => "Description",
        'Content.Type' => "Type",
        'Content.DocumentType' => "Type de document",
        'Content.Language' => "Langue",
        'Content.DescriptionLanguage' => "Langue de la description",
        'Content.Status' => "Etat",
        'Content.Version' => "Version",
        'Content.Source' => "Source",
        'Content.CreatedDate' => [
            'label' => "Date de création",
            'type' => 'datetime',
        ],
        'Content.TransactedDate' => [
            'label' => "Date de la transaction",
            'type' => 'datetime',
        ],
        'Content.AcquiredDate' => [
            'label' => "Date de numérisation",
            'type' => 'datetime',
        ],
        'Content.SentDate' => [
            'label' => "Date d'envoi",
            'type' => 'datetime',
        ],
        'Content.ReceivedDate' => [
            'label' => "Date de réception",
            'type' => 'datetime',
        ],
        'Content.RegisteredDate' => [
            'label' => "Date d'enregistrement",
            'type' => 'datetime',
        ],
        'Content.StartDate' => [
            'label' => "Date d'ouverture / date de début",
            'type' => 'datetime',
        ],
        'Content.EndDate' => [
            'label' => "Date de fermeture / Date de fin",
            'type' => 'datetime',
        ],
        'Content.DateLitteral' => "Champ date en texte libre.",
    ],
    $parent
)?>

<?php $helper->render('management_group.php', $path, 0, $helper->get('Management', $parent)); ?>

<?php
$history = null;
if ($helper->getAll('Content.CustodialHistory.CustodialHistoryItem', $parent)->count()) {
    $custodial = $helper->get('Content.CustodialHistory', $parent);
    $trs = [];
    /** @var DOMElement $item */
    foreach ($helper->getAll('CustodialHistoryItem', $custodial) as $item) {
        $value = $item->nodeValue;
        if ($value) {
            $trs[] = "<tr><th>Evénement</th><td>$value</td></tr>\n";
        }
    }
    $history = '<table><tbody>'.implode("\n", $trs).'</tbody></table>';
}
if ($history):
    ?>
    <div class="section">
        <h4 class="h2">Historique de conservation</h4>

        <?=$history?>
    </div>
<?php endif; ?>

<?php
$tags = null;
if ($helper->getAll('Content.Tag', $parent)->count()) {
    $trs = [];
    /** @var DOMElement $item */
    foreach ($helper->getAll('Content.Tag', $parent) as $tag) {
        $value = $tag->nodeValue;
        if ($value) {
            $trs[] = "<tr><th>Tag</th><td>$value</td></tr>\n";
        }
    }
    $tags = '<table><tbody>'.implode("\n", $trs).'</tbody></table>';
}
if ($tags):
    ?>
    <div class="section">
        <h4 class="h2">Tags</h4>

        <?=$tags?>
    </div>
<?php endif; ?>

<?php foreach ($helper->getAll('Content.Keyword', $parent) as $keyword):?>
<div class="section">
    <h4 class="h2">Mot-clé</h4>

    <?=$helper->table(
        [
            'KeywordContent' => "Libellé",
            'KeywordReference' => "Identifiant dans le référentiel associé",
            'KeywordType' => "Type",
        ],
        $keyword
    )?>

    <?php if ($helper->getAll('AccessRestrictionRule', $keyword)->count()): ?>
        <div class="section">
            <h5 class="h2">Communicabilité</h5>

            <?=$helper->table(
                [
                    'AccessRestrictionRule.Code' => "Code",
                    'AccessRestrictionRule.StartDate' => [
                        'label' => "Date de départ du calcul",
                        'type' => 'date',
                    ],
                ],
                $keyword
            )?>
        </div>
    <?php endif; ?>
</div>
<?php endforeach; ?>

<?php if ($helper->getAll('Content.Coverage', $parent)->count()): ?>
    <?php $coverage = $helper->get('Content.Coverage', $parent); ?>
    <div class="section">
        <h4 class="h2">Couverture spatiale, temporelle ou juridictionnelle de l’ArchiveUnit</h4>

        <?=$helper->table(
            [
                'Spatial' => "Couverture spatiale ou couverture géographique",
                'Temporal' => "Couverture temporelle",
                'Juridictional' => "Juridiction administrative ou ressort administratif",
            ],
            $coverage
        )?>
    </div>
<?php endif; ?>

<?php if ($helper->getAll('Content.OriginatingAgency', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Service producteur</h4>

    <?php $helper->render('entity.php', 'Content.OriginatingAgency', 0, $parent); ?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('Content.SubmissionAgency', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Opérateur de versement</h4>

    <?php $helper->render('entity.php', 'Content.SubmissionAgency', 0, $parent); ?>
</div>
<?php endif; ?>

<?php
$agents = [
    'AuthorizedAgent' => "Titulaire des droits de propriété intellectuelle",
    'Writer' => "Rédacteur de l'objet d'archive",
    'Addressee' => "Destinataire pour action",
    'Recipient' => "Destinataire pour information",
    'Transmitter' => "Emetteur du message",
    'Sender' => "Expéditeur du message",
];
foreach ($agents as $agent => $label):
    foreach ($helper->getAll('Content.'.$agent, $parent) as $agentNode):?>
        <div class="section">
            <h4 class="h2"><?=$label?></h4>

            <?php $helper->render(
                'agent_type.php',
                '',
                0,
                $agentNode
            ); ?>
        </div>
    <?php endforeach; ?>
<?php endforeach; ?>

<?php if ($gps = $helper->get('Content.Gps', $parent)): ?>
    <div class="section">
        <h4 class="h2">Gps</h4>

        <?=$helper->table(
            [
                'GpsVersionID' => "Identifiant de la version du GPS",
                'GpsAltitude' => "Indique l'altitude basée sur la référence dans GPSAltitudeRef",
                'GpsAltitudeRef' => "Indique l'altitude utilisée comme altitude de référence",
                'GpsLatitude' => "Mode d'expression de la latitude",
                'GpsLatitudeRef' => "Indique si la latitude est Nord ou Sud",
                'GpsLongitude' => "Mode d'expression de la longitude",
                'GpsLongitudeRef' => "Indique si la longitude est Est ou Ouest",
                'GpsDateStamp' => "Heure et Date de la position GPS",
            ],
            $gps
        )?>
    </div>
<?php endif; ?>

<?php
$events = '';
if ($helper->getAll('Content.Event', $parent)->count()) {
    $content = $helper->get('Content', $parent);
    $trs = [];
    /** @var DOMElement $event */
    foreach ($helper->getAll('Event', $content) as $event) {
        $events .= $helper->table(
            [
                'EventIdentifier' => "Identifiant de l'événement",
                'EventTypeCode' => "Code du type d'événement",
                'EventType' => "Type d'événement",
                'EventDateTime' => "Date et heure de l'événement",
                'EventDetail' => "Détail sur l'événement",
                'Outcome' => "Résultat du traitement",
                'OutcomeDetail' => "Détail sur le résultat du traitement",
                'OutcomeDetailMessage' => "Message détaillé sur le résultat du traitement",
                'EventDetailData' => "Message technique détaillant l'erreur",
                'EventAbstract' => "Résumé de l'événement",
            ],
            $event
        );
        $linkingAgentIdentifiers = $helper->getAll('LinkingAgentIdentifier', $event);
        if (!$linkingAgentIdentifiers->count()) {
            continue;
        }
        $linkingAgents = '';
        /** @var DOMElement $linkingAgentIdentifier */
        foreach ($linkingAgentIdentifiers as $linkingAgentIdentifier) {
            $linkingAgents .= $helper->table(
                [
                    'LinkingAgentIdentifierType' => "Identifiant d'un agent répertorié dans des évènements",
                    'LinkingAgentIdentifierValue' => "Mention d'un agent répertorié dans des évènements",
                    'LinkingAgentRole' => "Fonction d'un agent répertorié dans des évènements",
                ],
                $linkingAgentIdentifier
            );
        }
        if ($linkingAgents) {
            $events .= <<<HTML
    <div class="section">
        <h5 class="h3">Identifiant de l'agent lié</h5>
            $linkingAgents
    </div>
HTML;

        }
    }
}
if ($events):
    ?>
    <div class="section">
        <h4 class="h2">Evénement survenu au cours d'une procédure</h4>

        <?=$events?>
    </div>
<?php endif; ?>


<?php if ($gps = $helper->get('Content.Gps', $parent)): ?>
    <div class="section">
        <h4 class="h2">Gps</h4>

        <?=$helper->table(
            [
                'GpsVersionID' => "Identifiant de la version du GPS",
                'GpsAltitude' => "Indique l'altitude basée sur la référence dans GPSAltitudeRef",
                'GpsAltitudeRef' => "Indique l'altitude utilisée comme altitude de référence",
                'GpsLatitude' => "Mode d'expression de la latitude",
                'GpsLatitudeRef' => "Indique si la latitude est Nord ou Sud",
                'GpsLongitude' => "Mode d'expression de la longitude",
                'GpsLongitudeRef' => "Indique si la longitude est Est ou Ouest",
                'GpsDateStamp' => "Heure et Date de la position GPS",
            ],
            $gps
        )?>
    </div>
<?php endif; ?>

<?php
$links = $helper->getAll('DataObjectReference.DataObjectReferenceId', $parent);
if ($links->count()): ?>
    <div class="section">
        <h4 class="h2">Fichiers</h4>

        <?php foreach ($links as $link): ?>
            <a href="#<?=$link->nodeValue?>"><?=$helper->getValue('Content.Title', $parent)?></a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php
$archiveUnitKey = 0;
foreach ($helper->getAll('ArchiveUnit', $parent) as $archiveUnit) {
    $archiveUnitKey++;
    $helper->render('archive_unit.php', "$path.$index", $archiveUnitKey, $archiveUnit);
}
