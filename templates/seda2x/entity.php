<?php
/**
 * @var Seda2Pdf\Helper $helper
 */

$trs = [];
$label = "Identifiant";
$identifier = $helper->getValue($path.'.Identifier', $parent);
$trs[] = "<tr><th>$label</th><td>$identifier</td></tr>\n";
$metadatas = [
    'Description' => "Description",
    'Name' => "Nom",
];
$base = $helper->get($path, $parent);
foreach ($metadatas as $element => $label) {
    $node = $helper->xpath->query('ns:OrganizationDescriptiveMetadata/seda1:'.$element, $base)->item(0);
    if ($node) {
        $value = $node->nodeValue;
        $trs[] = "<tr><th>$label</th><td>$value</td></tr>\n";
    }
}
echo '<table><tbody>'.implode("\n", $trs).'</tbody></table>';
