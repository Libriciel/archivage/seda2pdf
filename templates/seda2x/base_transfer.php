<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
$identifier = $helper->getValue('MessageIdentifier');
$helper->xpath->registerNamespace('seda1', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$identifier?> - SEDA v2.1</title>
    <style type="text/css">
        <?php include '../style.css'; ?>
    </style>
</head>
<body>

<h1>Transfert -&nbsp;<?=$identifier?></h1>

<?=$helper->table(
    [
        'Comment' => "Commentaire",
        'Date' => "Date",
        'MessageIdentifier' => "Identifiant",
        'ArchivalAgreement' => "Accord de versement",
        'RelatedTransferReference' => "Référence complémentaire",
        'TransferRequestReplyIdentifier' => "Identifiant de la réponse à la demande de transfert",
    ]
)?>

<div class="section">
    <h2 class="h2">Service d'archives</h2>

    <?php $helper->render('entity.php', 'ArchivalAgency', 0, null); ?>
</div>

<div class="section">
    <h2 class="h2">Service versant</h2>

    <?php $helper->render('entity.php', 'TransferringAgency', 0, null); ?>
</div>

<?php $helper->render('archive.php', 'ArchivalAgency', 0, $helper->get('DataObjectPackage')); ?>

</body>
</html>
