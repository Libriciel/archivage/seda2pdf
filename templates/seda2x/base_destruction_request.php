<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
$identifier = $helper->getValue('MessageIdentifier');
$helper->xpath->registerNamespace('seda1', 'fr:gouv:culture:archivesdefrance:seda:v1.0');

$metadataNode = $helper->xpath->query('ns:Comment[2]')->item(0);
$metadata = $metadataNode ? $helper->metadataParser($metadataNode->nodeValue) : null;
$unitsMetadatas = $helper->xpath->query('ns:Comment[position() >= 3]');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$identifier?> - SEDA v2.1</title>
    <style type="text/css">
        <?php include '../style.css'; ?>
    </style>
</head>
<body>

<h1>Demande d'élimination -&nbsp;<?=$identifier?></h1>

<?=$helper->table(
    [
        'Comment' => "Commentaire",
        'Date' => [
            'label' => "Date",
            'type' => 'datetime',
        ],
        'MessageIdentifier' => "Identifiant",
        'AuthorizationRequestContent.AuthorizationReason' => "Motif de l'autorisation",
        'AuthorizationRequestContent.RequestDate' => [
            'label' => "Date de la demande d'autorisation",
            'type' => 'date',
        ],
    ]
)?>

<div class="section">
    <h2 class="h2">Service demandeur</h2>

    <?php $helper->render('entity.php', 'AuthorizationRequestContent.Requester', 0, null); ?>
</div>

<div class="section">
    <h2 class="h2">Service d'archives</h2>

    <?php $helper->render('entity.php', 'ArchivalAgency', 0, null); ?>
</div>

<div class="section">
    <h2 class="h2">Producteur</h2>

    <?php $helper->render('entity.php', 'OriginatingAgency', 0, null); ?>
</div>


<div class="section">
    <h2 class="h2">Unité d'archives</h2>
    <?php if ($metadata): ?>
    <table>
        <tbody>
            <tr><th>Nombre d'unité d'archives</th><td><?=$metadata['archive_units_count'] ?? ''?></td></tr>
            <tr><th>Nombre de fichiers</th><td><?=$metadata['original_count'] ?? ''?></td></tr>
            <tr><th>Taille totale des fichiers</th><td><?=$helper->readableSize($metadata['original_size'] ?? 0)?></td></tr>
        </tbody>
    </table>
    <?php endif; ?>

    <table>

        <?php
        if ($unitsMetadatas->count()) {
            echo '        <thead><tr><th>Identifiant</th><th>Nom</th><th>Nombre de fichiers</th><th>Taille des fichiers</th></tr></thead>'.PHP_EOL;
            echo '        <tbody>'.PHP_EOL;
            foreach ($unitsMetadatas as $metaNode) {
                $meta = $helper->metadataParser($metaNode->nodeValue);
                $identifier = $meta['archival_agency_identifier'] ?? null;
                $name = $meta['name'] ?? null;
                $fileSize = $helper->readableSize($meta['original_total_size'] ?? 0);
                $fileCount = $meta['original_total_count'] ?? null;
                echo "            <tr><td>$identifier</td><td>$name</td><td>$fileCount</td><td>$fileSize</td></tr>";
            }
        } else {
            echo '        <tbody>'.PHP_EOL;
            /** @var DOMElement $unit */
            foreach ($helper->getAll('AuthorizationRequestContent.UnitIdentifier') as $key => $unit) {
                echo '<tr><td>' . $unit->nodeValue . '</td></tr>' . PHP_EOL;
            }
            echo '        </tbody>'.PHP_EOL;
        }
        ?>
    </table>
</div>

</body>
</html>
