<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
?>

<?=$helper->table(
    [
        'FirstName' => "Prénom",
        'BirthName' => "Nom de naissance",
        'FullName' => "Nom complet",
        'GivenName' => "Nom d'usage",
        'Gender' => "Sexe/Genre",
        'BirthDate' => "Date de naissance",
        'DeathDate' => "Date du décès",
        'Nationality' => "Nationalité",
        'Corpname' => "Nom d'une entité",
        'Function' => "Fonction",
        'Activity' => "Activité",
        'Position' => "Intitulé du poste de travail",
        'Role' => "Rôle",
        'Mandate' => "Propriété intellectuelle et artistique",
    ],
    $parent
)?>

<?php if ($helper->getAll('BirthPlace')->count()): ?>
<div class="section">
    <h4 class="h2">BirthPlace</h4>

    <?=$helper->table(
        [
            'Geogname' => "Geogname",
            'Address' => "Address",
            'PostalCode' => "PostalCode",
            'City' => "City",
            'Region' => "Region",
            'Country' => "Country",
        ],
        $parent
    )?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('DeathPlace')->count()): ?>
<div class="section">
    <h4 class="h2">BirthPlace</h4>

    <?=$helper->table(
        [
            'Geogname' => "Geogname",
            'Address' => "Address",
            'PostalCode' => "PostalCode",
            'City' => "City",
            'Region' => "Region",
            'Country' => "Country",
        ],
        $parent
    )?>
</div>
<?php endif; ?>
