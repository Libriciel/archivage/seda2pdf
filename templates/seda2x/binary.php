<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
/** @var DOMElement $group */
$group = $helper->xpath->query('..', $parent)->item(0);
$groupId = '';
if ($group->nodeName === 'DataObjectGroup') {
    $groupId = ' id="'.$group->getAttribute('id').'"';
}
?>
<h3 class="h2"<?=$groupId?>>
    <a id="<?=$parent->getAttribute('id')?>">
        <?=basename($helper->getAttrValue('Attachment', 'filename', $parent))?>
    </a>
</h3>

<?=$helper->table(
    [
        'DataObjectGroupId' => "Identifiant du groupe",
        'DataObjectVersion' => "Version",
        'Attachment@filename' => [
            'label' => "Fichier",
            'type' => 'attribute',
        ],
        'MessageDigest' => "Empreinte",
        'MessageDigest@algorithm' => [
            'label' => "Algorithme",
            'type' => 'attribute',
        ],
        'Size' => [
            'label' => "Taille",
            'type' => 'size',
        ],
    ],
    $parent
)?>

<?php if ($helper->getAll('FormatIdentification', $parent)->count()): ?>
    <div class="section">
        <h4 class="h2">Identification du format</h4>

        <?=$helper->table(
            [
                'FormatIdentification.FormatLitteral' => "Forme littérale du nom du format",
                'FormatIdentification.MimeType' => "Type Mime associé",
                'FormatIdentification.FormatId' => "Type spécifique du format",
            ],
            $parent
        )?>
    </div>
<?php endif; ?>

<?php if ($helper->getAll('FileInfo', $parent)->count()): ?>
    <div class="section">
        <h4 class="h2">Information sur le fichier</h4>

        <?=$helper->table(
            [
                'FileInfo.Filename' => "Nom du fichier",
                'FileInfo.LastModified' => "Date de la dernière modification",
            ],
            $parent
        )?>
    </div>
<?php endif; ?>
