<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
$identifier = $helper->getValue('DescriptiveMetadata.ArchiveUnit.Content.Title');
$helper->xpath->registerNamespace('seda1', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$identifier?> - SEDA v2.1</title>
    <style type="text/css">
        <?php include '../style.css'; ?>
    </style>
</head>
<body>

<h1>Archive -&nbsp;<?=$identifier?></h1>

<?php $helper->render(
    'archive.php',
    'DescriptiveMetadata.ArchiveUnit',
    0,
    $helper->get('DataObjectPackage')
); ?>

</body>
</html>
