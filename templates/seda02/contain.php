<?php
/**
 * @var Seda2Pdf\Helper $helper
 * @var DOMElement $parent
 * @var int $index
 */
?>
<h4 class="h2">
    <a id="archive-object-<?=str_replace('.', '-', $path).'-'.$index?>">
        <?="$path.$index"?>. - <?=$helper->getValue('Name', $parent)?>
    </a>
</h4>

<?=$helper->table(
    [
        'ArchivalAgencyObjectIdentifier' => "Identifiant attribué par le service d'archives",
        'ArchivalAgreement' => "Accord de versement",
        'ArchivalProfile' => "Profil d'archives",
        'DescriptionLanguage' => "Langue de la description",
        'DescriptionLevel' => "Niveau de description",
        'Name' => "Nom de l'objet d'archive",
        'ServiceLevel' => "Niveau de service",
        'TransferringAgencyArchiveIdentifier' => "Identifiant attribué par le service versant",
    ],
    $parent
)?>

<?php if ($helper->getAll('ContentDescription', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Description de l'archive</h4>

    <?=$helper->table(
        [
            'ContentDescription.CustodialHistory' => "Historique de conservation",
            'ContentDescription.Description' => "Description",
            'ContentDescription.FilePlanPosition' => "Position dans le plan de classement",
            'ContentDescription.Format' => "Format",
            'ContentDescription.Language' => "Language",
            'ContentDescription.LatestDate' => [
                'label' => "Date de production la plus récente des objets-données.",
                'type' => 'date',
            ],
            'ContentDescription.OldestDate' => [
                'label' => "Date de production la plus ancienne des objets-données.",
                'type' => 'date',
            ],
            'ContentDescription.OtherDescriptiveData' => "Autres informations",
            'ContentDescription.RelatedObjectReference' => "Référence complémentaire",
            'ContentDescription.Size' => [
                'label' => "Taille",
                'type' => 'size',
            ],
        ],
        $parent
    )?>

    <?php if ($helper->getAll('ContentDescription.AccessRestrictionRule', $parent)->count()): ?>
        <div class="section">
            <h5 class="h2">Communicabilité</h5>

            <?=$helper->table(
                [
                    'ContentDescription.AccessRestrictionRule.Code' => "Code",
                    'ContentDescription.AccessRestrictionRule.StartDate' => [
                        'label' => "Date de départ du calcul",
                        'type' => 'date',
                    ],
                ],
                $parent
            )?>
        </div>
    <?php endif; ?>

    <?php
    $history = null;
    if ($helper->getAll('ContentDescription.CustodialHistory.CustodialHistoryItem', $parent)->count()) {
        $custodial = $helper->get('ContentDescription.CustodialHistory', $parent);
        $trs = [];
        /** @var DOMElement $item */
        foreach ($helper->getAll('CustodialHistoryItem', $custodial) as $item) {
            $value = $item->nodeValue;
            if ($value) {
                $trs[] = "<tr><th>Evénement</th><td>$value</td></tr>\n";
            }
        }
        $history = '<table><tbody>'.implode("\n", $trs).'</tbody></table>';
    }
    if ($history):
        ?>
        <div class="section">
            <h5 class="h2">Historique de conservation</h5>

            <?=$history?>
        </div>
    <?php endif; ?>

    <?php if ($helper->getAll('ContentDescription.OriginatingAgency', $parent)->count()): ?>
        <div class="section">
            <h5 class="h2">Service producteur</h5>

            <?=$helper->table(
                [
                    'ContentDescription.OriginatingAgency.BusinessType' => "Code de l'activité",
                    'ContentDescription.OriginatingAgency.Identification' => "Identifiant",
                    'ContentDescription.OriginatingAgency.LegalClassification' => "Code de la catégorie juridique",
                    'ContentDescription.OriginatingAgency.Name' => "Nom",
                ],
                $parent
            )?>

            <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Contact', $parent)->count()): ?>
                <div class="section">
                    <h6 class="h2">Contact</h6>

                    <?=$helper->table(
                        [
                            'ContentDescription.OriginatingAgency.Contact.PersonName' => "Nom",
                            'ContentDescription.OriginatingAgency.Contact.Identification' => "Identifiant",
                            'ContentDescription.OriginatingAgency.Contact.Responsibility' => "Attributions",
                            'ContentDescription.OriginatingAgency.Contact.DepartmentName' => "Service",
                        ],
                        $parent
                    )?>

                    <?php foreach ($helper->getAll('ContentDescription.OriginatingAgency.Contact.Address', $parent) as $adress): ?>
                        <div class="section">
                            <h6 class="h2">Adresse</h6>

                            <?=$helper->table(
                                [
                                    'BlockName' => "Quartier",
                                    'BuildingName' => "Bâtiment",
                                    'BuildingNumber' => "Numéro",
                                    'CityName' => "Localité",
                                    'CitySub-DivisionName' => "Arrondissement / quartier",
                                    'Country' => "Pays",
                                    'FloorIdentification' => "Etage",
                                    'Postcode' => "Code postal",
                                    'PostOfficeBox' => "Boite postale",
                                    'RoomIdentification' => "Pièce",
                                    'StreetName' => "Voie",
                                ],
                                $adress
                            )?>
                        </div>
                    <?php endforeach; ?>

                    <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Communication', $parent)->count()): ?>
                        <div class="section">
                            <h6 class="h2">Communication</h6>

                            <?=$helper->table(
                                [
                                    'ContentDescription.OriginatingAgency.Contact.Communication.Channel' => "Quartier",
                                    'ContentDescription.OriginatingAgency.Contact.Communication.CompleteNumber' => "QuartierQuartier",
                                    'ContentDescription.OriginatingAgency.Contact.Communication.URI' => "Identifiant ressource",
                                ],
                                $parent
                            )?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php foreach ($helper->getAll('ContentDescription.OriginatingAgency.Address', $parent) as $adress): ?>
                <div class="section">
                    <h6 class="h2">Adresse</h6>

                    <?=$helper->table(
                        [
                            'BlockName' => "Quartier",
                            'BuildingName' => "Bâtiment",
                            'BuildingNumber' => "Numéro",
                            'CityName' => "Localité",
                            'CitySub-DivisionName' => "Arrondissement / quartier",
                            'Country' => "Pays",
                            'FloorIdentification' => "Etage",
                            'Postcode' => "Code postal",
                            'PostOfficeBox' => "Boite postale",
                            'RoomIdentification' => "Pièce",
                            'StreetName' => "Voie",
                        ],
                        $adress
                    )?>
                </div>
            <?php endforeach; ?>

            <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Communication', $parent)->count()): ?>
                <div class="section">
                    <h6 class="h2">Communication</h6>

                    <?=$helper->table(
                        [
                            'ContentDescription.OriginatingAgency.Communication.Channel' => "Quartier",
                            'ContentDescription.OriginatingAgency.Communication.CompleteNumber' => "QuartierQuartier",
                            'ContentDescription.OriginatingAgency.Communication.URI' => "Identifiant ressource",
                        ],
                        $parent
                    )?>
                </div>
            <?php endif; ?>

        </div>
    <?php endif; ?>

    <?php foreach ($helper->getAll('ContentDescription.ContentDescriptive', $parent) as $keyword): ?>
        <div class="section">
            <h5 class="h2">Mot-clé</h5>

            <?=$helper->table(
                [
                    'KeywordContent' => "Libellé",
                    'KeywordReference' => "Identifiant dans le référentiel associé",
                    'KeywordType' => "Type",
                ],
                $keyword
            )?>

            <?php if ($helper->getAll('AccessRestrictionRule', $keyword)->count()): ?>
                <div class="section">
                    <h6 class="h2">Communicabilité</h6>

                    <?=$helper->table(
                        [
                            'AccessRestrictionRule.Code' => "Code",
                            'AccessRestrictionRule.StartDate' => [
                                'label' => "Date de départ du calcul",
                                'type' => 'date',
                            ],
                        ],
                        $keyword
                    )?>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('AccessRestriction', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Communicabilité</h4>

    <?=$helper->table(
        [
            'AccessRestriction.Code' => "Code",
            'AccessRestriction.StartDate' => [
                'label' => "Date de départ du calcul",
                'type' => 'date',
            ],
        ],
        $parent
    )?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('AppraisalRule', $parent)->count()): ?>
    <div class="section">
        <h4 class="h2">Sort final</h4>

        <?=$helper->table(
            [
                'AppraisalRule.Code' => "Code",
                'AppraisalRule.Duration' => "Durée d'utilité administrative",
                'AppraisalRule.StartDate' => [
                    'label' => "Date de départ du calcul",
                    'type' => 'date',
                ],
            ],
            $parent
        )?>
    </div>
<?php endif; ?>

<?php
$documentKey = 0;
foreach ($helper->getAll('Document', $parent) as $document) {
    $documentKey++;
    $helper->render('document.php', "$path.$index", $documentKey, $document);
}

$archiveObjectKey = 0;
foreach ($helper->getAll('Contains', $parent) as $archiveObject) {
    $archiveObjectKey++;
    $helper->render('contain.php', "$path.$index", $archiveObjectKey, $archiveObject);
}
