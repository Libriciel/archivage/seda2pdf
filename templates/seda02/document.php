<?php
/**
 * @var Seda2Pdf\Helper $helper
 * @var DOMElement $parent
 * @var string $path
 * @var int $index
 */
?>
<div class="section">
    <h5 class="h2">
        <a id="document-<?=str_replace('.', '-', $path).'-'.$index?>">
            <?="$path.$index"?> - <?=basename($helper->getAttrValue('Attachment', 'filename', $parent))?>
        </a>
    </h5>

    <?=$helper->table(
        [
            'Attachment@format' => [
                'label' => "Format",
                'type' => 'attribute',
            ],
            'Attachment@mimeCode' => [
                'label' => "Code MIME",
                'type' => 'attribute',
            ],
            'Attachment@filename' => [
                'label' => "Fichier",
                'type' => 'attribute',
            ],
        ],
        $parent
    )?>

    <?=$helper->table(
        [
            'Control' => "Présence d'exigences de contrôle",
            'Copy' => "Est une copie",
            'Creation' => [
                'label' => "Date de création",
                'type' => "datetime",
            ],
            'Description' => "Description",
            'Identification' => "Identifiant",
            'Issue' => [
                'label' => "Date d'émission",
                'type' => "datetime",
            ],
            'ItemIdentifier' => "Identifiant unique d'un élément particulier dans le document",
            'Purpose' => "Objet",
            'Receipt' => [
                'label' => "Date de réception",
                'type' => "datetime",
            ],
            'Response' => [
                'label' => "Date de réponse",
                'type' => "datetime",
            ],
            'Status' => "Etat",
            'Submission' => [
                'label' => "Date de soumission",
                'type' => "datetime",
            ],
            'Type' => "Type",
        ],
        $parent
    )?>
</div>
