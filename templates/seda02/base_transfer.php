<?php
/**
 * @var Seda2Pdf\Helper $helper
 */
$identifier = $helper->getValue('TransferIdentifier');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$identifier?> - SEDA v0.2</title>
    <style type="text/css">
        <?php include '../style.css'; ?>
    </style>
</head>
<body>

<h1>Transfert -&nbsp;<?=$identifier?></h1>

<?=$helper->table(
    [
        'Comment' => "Commentaire",
        'Date' => [
            'label' => "Date",
            'type' => 'date',
        ],
        'RelatedTransferReference' => "Référence complémentaire",
        'TransferIdentifier' => "Identifiant",
        'TransferRequestReplyIdentifier' => "Identifiant de la réponse à la demande de transfert",
    ]
)?>

<div class="section">
    <h2 class="h2">Service versant</h2>

    <?=$helper->table(
        [
            'TransferringAgency.BusinessType' => "Code de l'activité",
            'TransferringAgency.Identification' => "Identifiant",
            'TransferringAgency.LegalClassification' => "Code de la catégorie juridique",
            'TransferringAgency.Name' => "Nom",
        ]
    )?>

    <?php if ($helper->getAll('TransferringAgency.Contact')->count()): ?>
        <div class="section">
            <h3 class="h2">Contact</h3>

            <?=$helper->table(
                [
                    'TransferringAgency.Contact.DepartmentName' => "Service",
                    'TransferringAgency.Contact.Identification' => "Identifiant",
                    'TransferringAgency.Contact.PersonName' => "Nom",
                    'TransferringAgency.Contact.Responsibility' => "Attributions",
                ]
            )?>

            <?php foreach ($helper->getAll('TransferringAgency.Contact.Address') as $adress): ?>
                <div class="section">
                    <h4 class="h2">Adresse</h4>

                    <?=$helper->table(
                        [
                            'BlockName' => "Quartier",
                            'BuildingName' => "Bâtiment",
                            'BuildingNumber' => "Numéro",
                            'CityName' => "Localité",
                            'CitySub-DivisionName' => "Arrondissement / quartier",
                            'Country' => "Pays",
                            'FloorIdentification' => "Etage",
                            'Postcode' => "Code postal",
                            'PostOfficeBox' => "Boite postale",
                            'RoomIdentification' => "Pièce",
                            'StreetName' => "Voie",
                        ],
                        $adress
                    )?>
                </div>
            <?php endforeach; ?>

            <?php if ($helper->getAll('TransferringAgency.Communication')->count()): ?>
                <div class="section">
                    <h4 class="h2">Contact</h4>

                    <?=$helper->table(
                        [
                            'TransferringAgency.Contact.Communication.Channel' => "Quartier",
                            'TransferringAgency.Contact.Communication.CompleteNumber' => "QuartierQuartier",
                            'TransferringAgency.Contact.Communication.URI' => "Identifiant ressource",
                        ]
                    )?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php foreach ($helper->getAll('TransferringAgency.Address') as $adress): ?>
        <div class="section">
            <h3 class="h2">Adresse</h3>

            <?=$helper->table(
                [
                    'BlockName' => "Quartier",
                    'BuildingName' => "Bâtiment",
                    'BuildingNumber' => "Numéro",
                    'CityName' => "Localité",
                    'CitySub-DivisionName' => "Arrondissement / quartier",
                    'Country' => "Pays",
                    'FloorIdentification' => "Etage",
                    'Postcode' => "Code postal",
                    'PostOfficeBox' => "Boite postale",
                    'RoomIdentification' => "Pièce",
                    'StreetName' => "Voie",
                ],
                $adress
            )?>
        </div>
    <?php endforeach; ?>

    <?php if ($helper->getAll('TransferringAgency.Communication')->count()): ?>
        <div class="section">
            <h3 class="h2">Communication</h3>

            <?=$helper->table(
                [
                    'TransferringAgency.Communication.Channel' => "Quartier",
                    'TransferringAgency.Communication.CompleteNumber' => "QuartierQuartier",
                    'TransferringAgency.Communication.URI' => "Identifiant ressource",
                ]
            )?>
        </div>
    <?php endif; ?>

</div>

<div class="section">
    <h2 class="h2">Service d'archives</h2>

    <?=$helper->table(
        [
            'TransferringAgency.BusinessType' => "Code de l'activité",
            'TransferringAgency.Identification' => "Identifiant",
            'TransferringAgency.LegalClassification' => "Code de la catégorie juridique",
            'TransferringAgency.Name' => "Nom",
        ]
    )?>

    <?php foreach ($helper->getAll('ArchivalAgency.Address') as $adress): ?>
        <div class="section">
            <h3 class="h2">Adresse</h3>

            <?=$helper->table(
                [
                    'BlockName' => "Quartier",
                    'BuildingName' => "Bâtiment",
                    'BuildingNumber' => "Numéro",
                    'StreetName' => "Voie",
                    'Postcode' => "Code postal",
                    'CityName' => "Localité",
                    'CitySub-DivisionName' => "Arrondissement / quartier",
                    'Country' => "Pays",
                    'FloorIdentification' => "Etage",
                    'PostOfficeBox' => "Boite postale",
                    'RoomIdentification' => "Pièce",
                ],
                $adress
            )?>
        </div>
    <?php endforeach; ?>

    <?php if ($helper->getAll('ArchivalAgency.Communication')->count()): ?>
        <div class="section">
            <h3 class="h2">Communication</h3>

            <?=$helper->table(
                [
                    'ArchivalAgency.Communication.Channel' => "Quartier",
                    'ArchivalAgency.Communication.CompleteNumber' => "QuartierQuartier",
                    'ArchivalAgency.Communication.URI' => "Identifiant ressource",
                ]
            )?>
        </div>
    <?php endif; ?>

    <?php if ($helper->getAll('ArchivalAgency.Contact')->count()): ?>
        <div class="section">
            <h3 class="h2">Contact</h3>

            <?=$helper->table(
                [
                    'ArchivalAgency.OriginatingAgency.Contact.PersonName' => "Nom",
                    'ArchivalAgency.OriginatingAgency.Contact.Identification' => "Identifiant",
                    'ArchivalAgency.OriginatingAgency.Contact.Responsibility' => "Attributions",
                    'ArchivalAgency.OriginatingAgency.Contact.DepartmentName' => "Service",
                ]
            )?>

            <?php foreach ($helper->getAll('ArchivalAgency.Contact.Address') as $adress): ?>
                <div class="section">
                    <h4 class="h2">Adresse</h4>

                    <?=$helper->table(
                        [
                            'BlockName' => "Quartier",
                            'BuildingName' => "Bâtiment",
                            'BuildingNumber' => "Numéro",
                            'CityName' => "Localité",
                            'CitySub-DivisionName' => "Arrondissement / quartier",
                            'Country' => "Pays",
                            'FloorIdentification' => "Etage",
                            'Postcode' => "Code postal",
                            'PostOfficeBox' => "Boite postale",
                            'RoomIdentification' => "Pièce",
                            'StreetName' => "Voie",
                        ],
                        $adress
                    )?>
                </div>
            <?php endforeach; ?>

            <?php if ($helper->getAll('ArchivalAgency.Communication')->count()): ?>
                <div class="section">
                    <h4 class="h2">Communication</h4>

                    <?=$helper->table(
                        [
                            'ArchivalAgency.Contact.Communication.Channel' => "Quartier",
                            'ArchivalAgency.Contact.Communication.CompleteNumber' => "QuartierQuartier",
                            'ArchivalAgency.Contact.Communication.URI' => "Identifiant ressource",
                        ]
                    )?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>

<?php foreach ($helper->getAll('Integrity') as $integrity): ?>
    <div class="section">
        <h2 class="h2">Integrité -&nbsp;<?=$helper->getValue('UnitIdentifier', $integrity)?></h2>

        <?=$helper->table(
            [
                'UnitIdentifier' => "Nom de fichier",
                'Contains@algorithme' => [
                    'label' => "Algorithme de hashage",
                    'type' => 'attribute',
                ],
                'Contains' => "Hash d'un fichier"
            ],
            $integrity
        )?>
    </div>
<?php endforeach; ?>

<div class="page-break"></div>

<?php
$archiveKey = 0;
foreach ($helper->getAll('Contains') as $archive) {
    $archiveKey++;
    $helper->render('archive.php', '', $archiveKey, $archive);
}
?>

</body>
</html>
