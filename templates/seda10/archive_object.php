<?php
/**
 * @var Seda2Pdf\Helper $helper
 * @var DOMElement $parent
 * @var int $index
 */
?>
<h3 class="h2">
    <a id="archive-object-<?=str_replace('.', '-', $path).'-'.$index?>">
        <?="$path.$index"?>. - <?=$helper->getValue('Name', $parent)?>
    </a>
</h3>

<?=$helper->table(
    [
        'ArchivalAgencyObjectIdentifier' => "Identifiant attribué par le service d'archives",
        'TransferringAgencyObjectIdentifier' => "Identifiant attribué par le service versant",
        'OriginatingAgencyObjectIdentifier' => "Identifiant attribué par le service producteur",
        'ArchivalAgreement' => "Accord de versement",
        'ArchivalProfile' => "Profil d'archives",
        'DescriptionLanguage' => "Langue de la description",
        'Name' => "Nom de l'objet d'archive",
        'ServiceLevel' => "Niveau de service",
    ],
    $parent
)?>

<?php if ($helper->getAll('ContentDescription', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Description de l'objet d'archive</h4>

    <?=$helper->table(
        [
            'ContentDescription.Description' => "Description",
            'ContentDescription.DescriptionLevel' => "Niveau de description",
            'ContentDescription.FilePlanPosition' => "Position dans le plan de classement",
            'ContentDescription.Language' => "Language",
            'ContentDescription.LatestDate' => [
                'label' => "Date de production la plus récente des objets-données.",
                'type' => 'date',
            ],
            'ContentDescription.OldestDate' => [
                'label' => "Date de production la plus ancienne des objets-données.",
                'type' => 'date',
            ],
            'ContentDescription.OtherDescriptiveData' => "Autres informations",
        ],
        $parent
    )?>

    <?php if ($helper->getAll('ContentDescription.AccessRestrictionRule', $parent)->count()): ?>
        <div class="section">
            <h5 class="h2">Communicabilité</h5>

            <?=$helper->table(
                [
                    'ContentDescription.AccessRestrictionRule.Code' => "Code",
                    'ContentDescription.AccessRestrictionRule.StartDate' => [
                        'label' => "Date de départ du calcul",
                        'type' => 'date',
                    ],
                ],
                $parent
            )?>
        </div>
    <?php endif; ?>

    <?php
    $history = null;
    if ($helper->getAll('ContentDescription.CustodialHistory.CustodialHistoryItem', $parent)->count()) {
        $custodial = $helper->get('ContentDescription.CustodialHistory', $parent);
        $trs = [];
        /** @var DOMElement $item */
        foreach ($helper->getAll('CustodialHistoryItem', $custodial) as $item) {
            $value = $item->nodeValue;
            if ($value) {
                $trs[] = "<tr><th>Evénement</th><td>$value</td></tr>\n";
            }
        }
        $history = '<table><tbody>'.implode("\n", $trs).'</tbody></table>';
    }
    if ($history):
        ?>
        <div class="section">
            <h5 class="h2">Historique de conservation</h5>

            <?=$history?>
        </div>
    <?php endif; ?>

    <?php foreach ($helper->getAll('ContentDescription.Keyword', $parent) as $keyword):?>
        <div class="section">
            <h5 class="h2">Mot-clé</h5>

            <?=$helper->table(
                [
                    'KeywordContent' => "Libellé",
                    'KeywordReference' => "Identifiant dans le référentiel associé",
                    'KeywordType' => "Type",
                ],
                $keyword
            )?>

            <?php if ($helper->getAll('AccessRestrictionRule', $keyword)->count()): ?>
                <div class="section">
                    <h6 class="h2">Communicabilité</h6>

                    <?=$helper->table(
                        [
                            'AccessRestrictionRule.Code' => "Code",
                            'AccessRestrictionRule.StartDate' => [
                                'label' => "Date de départ du calcul",
                                'type' => 'date',
                            ],
                        ],
                        $keyword
                    )?>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>

    <?php if ($helper->getAll('ContentDescription.OriginatingAgency', $parent)->count()): ?>
        <div class="section">
            <h5 class="h2">Service producteur</h5>

            <?=$helper->table(
                [
                    'ContentDescription.OriginatingAgency.BusinessType' => "Code de l'activité",
                    'ContentDescription.OriginatingAgency.Description' => "Description",
                    'ContentDescription.OriginatingAgency.Identification' => "Identifiant",
                    'ContentDescription.OriginatingAgency.LegalClassification' => "Code de la catégorie juridique",
                    'ContentDescription.OriginatingAgency.Name' => "Nom",
                ],
                $parent
            )?>

            <?php foreach ($helper->getAll('ContentDescription.OriginatingAgency.Address', $parent) as $adress): ?>
                <div class="section">
                    <h6 class="h2">Adresse</h6>

                    <?=$helper->table(
                        [
                            'BlockName' => "Quartier",
                            'BuildingName' => "Bâtiment",
                            'BuildingNumber' => "Numéro",
                            'StreetName' => "Voie",
                            'Postcode' => "Code postal",
                            'CityName' => "Localité",
                            'CitySub-DivisionName' => "Arrondissement / quartier",
                            'Country' => "Pays",
                            'FloorIdentification' => "Etage",
                            'PostOfficeBox' => "Boite postale",
                            'RoomIdentification' => "Pièce",
                        ],
                        $adress
                    )?>
                </div>
            <?php endforeach; ?>

            <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Communication', $parent)->count()): ?>
                <div class="section">
                    <h6 class="h2">Communication</h6>

                    <?=$helper->table(
                        [
                            'ContentDescription.OriginatingAgency.Communication.Channel' => "Quartier",
                            'ContentDescription.OriginatingAgency.Communication.CompleteNumber' => "QuartierQuartier",
                            'ContentDescription.OriginatingAgency.Communication.URIID' => "Identifiant ressource",
                        ],
                        $parent
                    )?>
                </div>
            <?php endif; ?>

            <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Contact', $parent)->count()): ?>
                <div class="section">
                    <h6 class="h2">Contact</h6>

                    <?=$helper->table(
                        [
                            'ContentDescription.OriginatingAgency.Contact.PersonName' => "Nom",
                            'ContentDescription.OriginatingAgency.Contact.Identification' => "Identifiant",
                            'ContentDescription.OriginatingAgency.Contact.Responsibility' => "Attributions",
                            'ContentDescription.OriginatingAgency.Contact.DepartmentName' => "Service",
                        ],
                        $parent
                    )?>

                    <?php foreach ($helper->getAll('ContentDescription.OriginatingAgency.Contact.Address', $parent) as $adress): ?>
                        <div class="section">
                            <h7 class="h2">Adresse</h7>

                            <?=$helper->table(
                                [
                                    'BlockName' => "Quartier",
                                    'BuildingName' => "Bâtiment",
                                    'BuildingNumber' => "Numéro",
                                    'StreetName' => "Voie",
                                    'Postcode' => "Code postal",
                                    'CityName' => "Localité",
                                    'CitySub-DivisionName' => "Arrondissement / quartier",
                                    'Country' => "Pays",
                                    'FloorIdentification' => "Etage",
                                    'PostOfficeBox' => "Boite postale",
                                    'RoomIdentification' => "Pièce",
                                ],
                                $adress
                            )?>
                        </div>
                    <?php endforeach; ?>

                    <?php if ($helper->getAll('ContentDescription.OriginatingAgency.Communication', $parent)->count()): ?>
                        <div class="section">
                            <h7 class="h2">Communication</h7>

                            <?=$helper->table(
                                [
                                    'ContentDescription.OriginatingAgency.Contact.Communication.Channel' => "Quartier",
                                    'ContentDescription.OriginatingAgency.Contact.Communication.CompleteNumber' => "QuartierQuartier",
                                    'ContentDescription.OriginatingAgency.Contact.Communication.URIID' => "Identifiant ressource",
                                ],
                                $parent
                            )?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($helper->getAll('ContentDescription.RelatedObjectReference', $parent)->count()): ?>
        <div class="section">
            <h5 class="h2">Référence complémentaire</h5>

            <?=$helper->table(
                [
                    'ContentDescription.RelatedObjectReference.RelatedObjectIdentifier' => "Identifiant",
                    'ContentDescription.RelatedObjectReference.Relation' => "Nature de la relation",
                ],
                $parent
            )?>
        </div>
    <?php endif; ?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('AccessRestrictionRule', $parent)->count()): ?>
<div class="section">
    <h4 class="h2">Communicabilité</h4>

    <?=$helper->table(
        [
            'AccessRestrictionRule.Code' => "Code",
            'AccessRestrictionRule.StartDate' => [
                'label' => "Date de départ du calcul",
                'type' => 'date',
            ],
        ],
        $parent
    )?>
</div>
<?php endif; ?>

<?php if ($helper->getAll('AppraisalRule', $parent)->count()): ?>
    <div class="section">
        <h4 class="h2">Sort final</h4>

        <?=$helper->table(
            [
                'AppraisalRule.Code' => "Code",
                'AppraisalRule.Duration' => "Durée d'utilité administrative",
                'AppraisalRule.StartDate' => [
                    'label' => "Date de départ du calcul",
                    'type' => 'date',
                ],
            ],
            $parent
        )?>
    </div>
<?php endif; ?>

<?php
$documentKey = 0;
foreach ($helper->getAll('Document', $parent) as $document) {
    $documentKey++;
    $helper->render('document.php', "$path.$index", $documentKey, $document);
}

$archiveObjectKey = 0;
foreach ($helper->getAll('ArchiveObject', $parent) as $archiveObject) {
    $archiveObjectKey++;
    $helper->render('archive_object.php', "$path.$index", $archiveObjectKey, $archiveObject);
}
