<?php
/**
 * @var Seda2Pdf\Helper $helper
 * @var DOMElement $parent
 * @var string $path
 * @var int $index
 */
?>
<div class="section">
    <h5 class="h2">
        <a id="document-<?=str_replace('.', '-', $path).'-'.$index?>">
            <?="$path.$index"?> - <?=basename($helper->getAttrValue('Attachment', 'filename', $parent))?>
        </a>
    </h5>

    <?=$helper->table(
        [
            'Attachment@format' => [
                'label' => "Format",
                'type' => 'attribute',
            ],
            'Attachment@mimeCode' => [
                'label' => "Code MIME",
                'type' => 'attribute',
            ],
            'Attachment@filename' => [
                'label' => "Fichier",
                'type' => 'attribute',
            ],
        ],
        $parent
    )?>

    <?=$helper->table(
        [
            'Integrity' => 'Hash',
            'Integrity@algorithme' => [
                'label' => "Algoritme de hashage",
                'type' => 'attribute',
            ],
        ],
        $parent
    )?>

    <?=$helper->table(
        [
            'ArchivalAgencyDocumentIdentifier' => "Identifiant attribué par le service d'archives",
            'TransferringAgencyDocumentIdentifier' => "Identifiant attribué par le service versant",
            'OriginatingAgencyDocumentIdentifier' => "Identifiant attribué par le service producteur",
            'Control' => "Exigences de contrôle",
            'Copy' => "Exemplaire",
            'Creation' => [
                'label' => "Date de création",
                'type' => "datetime",
            ],
            'Description' => "Description",
            'Issue' => [
                'label' => "Date d'émission",
                'type' => "datetime",
            ],
            'Language' => "Langue du contenu",
            'Purpose' => "Objet",
            'Receipt' => [
                'label' => "Date de réception",
                'type' => "datetime",
            ],
            'Response' => [
                'label' => "Date de réponse",
                'type' => "datetime",
            ],
            'Size' => [
                'label' => "Taille",
                'type' => 'size',
            ],
            'Status' => "Etat",
            'Submission' => [
                'label' => "Date de soumission",
                'type' => "datetime",
            ],
            'Type' => "Type",
        ],
        $parent
    )?>
</div>
